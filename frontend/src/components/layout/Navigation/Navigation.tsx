import * as React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styled from '@emotion/styled'
import VisuallyHidden from '@reach/visually-hidden'

import { themeProps, Box, UnstyledAnchor, UnstyledButton, Text } from 'components/design-system'
import { logEventClick } from 'utils/analytics'
import useDarkMode from 'utils/useDarkMode'
import Logo from './Logo'
import NavLink from './NavLink'
import SearchModal from './SearchModal'
import { NavGrid, NavInner } from './NavComponents'
// import SearchIcon from './SearchIcon';

const StyledHeader = Box.withComponent('header')

const Root = styled(StyledHeader)``

const Left = styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100px;
  padding: 00;
  user-select: none;

  ${themeProps.mediaQueries.lg} {
    padding: 0 24px;
    padding-left: 0;
  }
`

const Center = styled('div')`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  height: 100px;
  padding: 0;
  user-select: none;
  flex: 1 1 auto;

  ${themeProps.mediaQueries.lg} {
    padding: 0 24px;
    padding-left: 0;
  }
`

const Right = styled('nav')`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100px;

  ${themeProps.mediaQueries.lg} {
    justify-content: flex-end;
  }
`

const ToggleButton = styled(UnstyledButton)`
  outline: none;
`

// const SearchButton = styled(UnstyledButton)`
//   display: inline-flex;
//   align-items: center;
//   justify-content: center;
//   width: 40px;
//   height: 40px;
// `;

const Navigation: React.FC = () => {
  const [isDarkMode, toggleDarkMode] = useDarkMode()
  const [isSearchModalOpen, setIsSearchModalOpen] = React.useState(false)
  const router = useRouter()

  const toggleSearchModal = () => {
    setIsSearchModalOpen(!isSearchModalOpen)
  }

  return (
    <Root>
      <NavGrid backgroundColor="accents01" color="foreground">
        <NavInner display="flex" flexDirection="row">
          <Left>
            <Link href="/" passHref>
              <UnstyledAnchor display="inline-flex" alignItems="center" onClick={() => logEventClick('Beranda')} fontWeight={600}>
                <VisuallyHidden>Kawal COVID-19</VisuallyHidden>
                <Logo aria-hidden />
              </UnstyledAnchor>
            </Link>
          </Left>
          <Center>
            <Box as="nav" display="flex" flexDirection="row" gridColumn="3/4" overflowX="auto" overflowY="hidden">
              <NavLink href="/" isActive={router.pathname === '/'} title="Beranda" />
              <NavLink href="/periksa-mandiri" isActive={router.pathname === '/periksa-mandiri'} title="Periksa Mandiri" />
              <NavLink href="/rs-rujukan" isActive={router.pathname === '/rs-rujukan'} title="RS Rujukan" />
              <NavLink href="/panduan" isActive={router.pathname === '/panduan'} title="Panduan" />
            </Box>
          </Center>
          <Right>
            <Text variant={200} as="p" color="foreground">
              Mode Warna
            </Text>
            <ToggleButton
              type="button"
              ml="sm"
              backgroundColor={isDarkMode ? 'accents03' : 'primary01'}
              fontSize="12px"
              fontWeight={isDarkMode ? 'normal' : '600'}
              color="#f1f2f3"
              py="xs"
              px="md"
              borderTopLeftRadius={50}
              borderBottomLeftRadius={50}
              onClick={toggleDarkMode}
            >
              Terang
            </ToggleButton>
            <ToggleButton
              type="button"
              backgroundColor={!isDarkMode ? 'accents03' : 'primary01'}
              fontSize="12px"
              fontWeight={!isDarkMode ? 'normal' : '600'}
              color="#f1f2f3"
              py="xs"
              px="md"
              borderTopRightRadius={50}
              borderBottomRightRadius={50}
              onClick={toggleDarkMode}
            >
              Gelap
            </ToggleButton>
            {/* <SearchButton type="button" backgroundColor="accents01" onClick={toggleSearchModal}>
              <SearchIcon fill={isDarkMode ? '#f1f2f3' : '#22272c'} />
            </SearchButton> */}
          </Right>
        </NavInner>
      </NavGrid>
      <SearchModal isOpen={isSearchModalOpen} onClose={toggleSearchModal} />
    </Root>
  )
}

export default Navigation
