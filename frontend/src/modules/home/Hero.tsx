import * as React from 'react'
import styled from '@emotion/styled'
import { themeProps, Heading, Paragraph, Stack, Box } from 'components/design-system'
import { Column, Content } from 'components/layout'

const Section = Content.withComponent('section')

const Root = styled(Section)`
  padding: 6vh 24px;

  ${themeProps.mediaQueries.lg} {
    padding: 8vh 24px;
  }
`

const Hero: React.FC = () => {
  return (
    <Root noPadding>
      <Column>
        <Box textAlign="center">
          <Stack spacing="xxl">
            <Heading variant={900} mb="lg" as="h1">
              Periksa kesehatan mandiri di sini dan bantu cegah penyebaran COVID-19
            </Heading>
            <Paragraph variant={500}>
              Semakin cepat tahu kondisi kesehatan, semakin mudah ambil tindakan yang mencegah penyebaran.
            </Paragraph>
          </Stack>
        </Box>
      </Column>
    </Root>
  )
}

export default Hero
