import { Stack, Heading, Box, themeProps, UnstyledAnchor } from 'components/design-system'
import styled from '@emotion/styled-base'
import Link from 'next/link'

export interface ThumbnailSectionProps {
  title: string
  items: ThumbnailBoxProps[]
}

export interface ThumbnailBoxProps {
  title: string
  description: string
  icon: React.ReactNode
  link: string
}

const ThumbnailSection: React.FC<ThumbnailSectionProps> = ({ title, items }) => (
  <Stack spacing="xl" mb="xxl">
    <Stack spacing="xl" mb="lg">
      <Heading variant={800} as="h2" textAlign="center">
        {title}
      </Heading>
      <Box>
        <GridWrapper>
          {items.map((data: ThumbnailBoxProps) => {
            const { title, description, icon, link } = data
            return <ThumbnailBox key={`${title}-thumbnail`} title={title} description={description} icon={icon} link={link} />
          })}
        </GridWrapper>
      </Box>
    </Stack>
  </Stack>
)

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: 24px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(auto-fill, minmax(calc(${themeProps.widths.md}px / 1 - 48px), 1fr));
  }

  ${themeProps.mediaQueries.lg} {
    grid-template-columns: repeat(auto-fill, minmax(calc(${themeProps.widths.lg}px / 3 - 48px), 1fr));
  }

  ${themeProps.mediaQueries.xl} {
    grid-template-columns: repeat(auto-fill, minmax(calc(${themeProps.widths.xl}px / 3 - 48px), 1fr));
  }
`

const ThumbnailLink = styled(UnstyledAnchor)`
  box-shadow: unset;

  &:hover,
  &:focus,
  &:active,
  &:visited {
    box-shadow: unset;
  }

  &:hover,
  &:focus {
    h3 {
      text-decoration: underline;
    }
  }
`

const ThumbnailBox: React.FC<ThumbnailBoxProps> = ({ title, description, icon, link }) => (
  <Box display="flex" justifyContent="center" borderRadius={6} backgroundColor="accents02">
    <Link href={link} as={link} passHref>
      <ThumbnailLink flex="1 1 auto" px="md" pt="md" pb="lg">
        <Box textAlign="center">
          <Stack spacing="md" p="md">
            <Box marginBottom={10}>{icon}</Box>
            <Box as="header">
              <Heading variant={600} as="h3">
                {title}
              </Heading>
            </Box>
            <Box>{description}</Box>
          </Stack>
        </Box>
      </ThumbnailLink>
    </Link>
  </Box>
)

export default ThumbnailSection
