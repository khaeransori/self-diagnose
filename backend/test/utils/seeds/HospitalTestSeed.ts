export const HospitalSeed = [
    {
        id: 1,
        name: "RSU Dr. Zainoel Abidin Banda Aceh",
        province: "Aceh",
        city: "Lhokseumawe",
        address: "Jl. Tgk. Daud Beureueh No.108 banda Aceh Telp: 0651 - 22077, 28148",
        phone: "0651-22077",
        description: "Tidak ada di data kemenkes",
        lat: -0.943174,
        long: 100.366944,
    },
];
