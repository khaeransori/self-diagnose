import { Connection } from "typeorm";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { HospitalService } from "../../../src/services/HospitalService";
import { HospitalRepository } from "../../../src/repositories/HospitalRepository";
import { Hospital } from "../../../src/entities/Hospital";
import { HospitalSeed } from "../../utils/seeds/HospitalTestSeed";
import { expressToOpenAPIPath } from "routing-controllers-openapi";

describe("HospitalService", () => {
    let db: Connection;
    let hospitalRepository: HospitalRepository;
    let hospitalService: HospitalService;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        hospitalRepository = db.getCustomRepository(HospitalRepository);
        hospitalService = new HospitalService(hospitalRepository);
        await hospitalRepository.save(HospitalSeed);
    });

    afterAll(() => db.close());

    const request = {
        province: "Aceh",
        city: "Lhokseumawe",
    };

    it("Get all hospital", async () => {
        const hospitals = await hospitalService.getHospitals();
        expect(hospitals).not.toBe(null);
        expect(hospitals[0]).toBeInstanceOf(Object);
    });

    it("Get hospital by province", async () => {
        const hospitals = await hospitalService.getHospitalsByProvince(request.province);
        expect(hospitals[0].province).toBe(request.province);
    });

    it("Get hospital by city", async () => {
        const hospitals = await hospitalService.getHospitalsByCity(request.city);
        expect(hospitals[0].city).toBe(request.city);
    });
});
