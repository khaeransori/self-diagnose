import { Connection } from "typeorm";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { UserService } from "../../../src/services/UserService";
import { UserRepository } from "../../../src/repositories/UserRepository";
import { User } from "../../../src/entities/User";

describe("UserService", () => {
    let db: Connection;
    let userRepository: UserRepository;
    let userService: UserService;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        userRepository = db.getCustomRepository(UserRepository);
        userService = new UserService(userRepository);
    });

    afterAll(() => db.close());

    const userRequest = {
        realName: "Hello jest",
        email: "hellojest@gmail.com",
        password: "password",
    };

    let newUserId: string;

    it("Create a user and return the created information", async () => {
        const newUser = await userService.createUser(userRequest as any);
        newUserId = newUser.id;
        expect(newUser.realName).toBe(userRequest.realName);
        expect(newUser.email).toBe(userRequest.email);
    });

    it("Id finds the matching user and returns the user information", async () => {
        const user = await userService.getUsersById(newUserId);
        expect(user).toBeInstanceOf(User);
        expect(user.realName).toBe(userRequest.realName);
        expect(user.email).toBe(userRequest.email);
    });

    it("Returns true if it checks for duplicates of already registered emails", async () => {
        const isDuplicateUser = await userService.isDuplicateUser(userRequest.email);
        expect(isDuplicateUser).toBeTruthy();
    });

    it("Checking for duplicate emails that are not registered returns false", async () => {
        const isUnDuplicateUser = await userService.isDuplicateUser("null@gmail.com");
        expect(isUnDuplicateUser).toBeFalsy();
    });
});
