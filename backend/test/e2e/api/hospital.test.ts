import app from "../utils/testApp";
import { agent, Response } from "supertest";
import { Connection } from "typeorm";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { HospitalRepository } from "../../../src/repositories/HospitalRepository";
import { HospitalSeed } from "../../utils/seeds/HospitalTestSeed";

let db: Connection;
let hospitalRepository: HospitalRepository;

const responseMessage = {
    RESULT_NOT_FOUND: "RESULT_NOT_FOUND",
};

beforeAll(async () => {
    db = await createMemoryDatabase();
    hospitalRepository = db.getCustomRepository(HospitalRepository);
    await hospitalRepository.save(HospitalSeed);
});

afterAll(async done => {
    await db.close();
    done();
});

describe("GET /api/hospitals", () => {
    it("200: Successfully return all hospital", done => {
        agent(app)
            .get("/api/hospitals")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Successfully return hospital by province", done => {
        agent(app)
            .get("/api/hospitals/province?name=aceh")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Get hospital by unknown province", done => {
        agent(app)
            .get("/api/hospitals/province?name=halo")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).toStrictEqual([]);
                done();
            });
    });

    it("200: Get hospital by city", done => {
        agent(app)
            .get("/api/hospitals/city?name=lhokseumawe")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).not.toBe(null);
                done();
            });
    });

    it("200: Get hospital by unknown city", done => {
        agent(app)
            .get("/api/hospitals/city?name=halo")
            .expect(200)
            .end((err: any, res: Response) => {
                if (err) return done(err);
                const { body } = res;
                expect(body).toStrictEqual([]);
                done();
            });
    });
});
