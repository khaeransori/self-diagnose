import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { HospitalRepository } from "../repositories/HospitalRepository";
import { Hospital } from "../entities/Hospital";

@Service()
export class HospitalService {
    constructor(@InjectRepository() private hospitalRepository: HospitalRepository) {}

    // Get list of all hospitals
    public async getHospitals() {
        var hospitals: Array<Hospital>;
        hospitals = await this.hospitalRepository.getHospitals();
        return hospitals;
    }

    // Filter hospitals by province
    public async getHospitalsByProvince(province: string) {
        var hospitals: Array<Hospital>;
        hospitals = await this.hospitalRepository.getHospitalsByProvince(province);
        return hospitals;
    }

    // Filter hospitals by city
    public async getHospitalsByCity(city: string) {
        var hospitals: Array<Hospital>;
        hospitals = await this.hospitalRepository.getHospitalsByCity(city);
        return hospitals;
    }
}
