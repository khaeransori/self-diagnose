import NodeCache from "node-cache";

// stdTTL: time to live in seconds for every generated cache element.
export class Cache{

  cache: NodeCache;

  constructor(ttl){
    this.cache = new NodeCache({ stdTTL: ttl })
  }

  set(key: string, val: any){
    return this.cache.set(key,val);
  }

  get(key: string) : any {
    return this.cache.get(key);
  }

  del(key: string){
    return this.cache.del(key);
  }

  has(key: string) : boolean {
    return this.cache.has(key);
  }

  flush(){
    return this.cache.flushAll();
  }
}

// Insantiate cache singleton with TTL h * 3600, please adjust accordingly
// TTL = 6 hour
const cache: Cache = new Cache(6 * 3600);

export { cache };