import { JsonController, Get, Res, HttpCode, QueryParam, Param, QueryParams } from "routing-controllers";
import { Response } from "express";
import { OpenAPI, ResponseSchema } from "routing-controllers-openapi";
import { HospitalService } from "../services/HospitalService";
import { logger } from "../utils/Logger";
import { Hospital } from "../entities/Hospital";

const responseMessage = {
    RESULT_NOT_FOUND: "RESULT_NOT_FOUND",
    RESULT_NOT_FOUND_ON_PROVINCE: "RESULT_NOT_FOUND_ON_PROVINCE",
    RESULT_NOT_FOUND_ON_CITY: "RESULT_NOT_FOUND_ON_CITY",
};

@JsonController("/hospitals")
export class HospitalController {
    constructor(private hospitalService: HospitalService) {}

    @HttpCode(200)
    @Get("")
    @ResponseSchema(Hospital)
    @OpenAPI({
        summary: "List reference hospital",
        description: "This route provide all reference hospitals",
        responses: {
            "200": {
                description: "return list of hospital",
            },
        },
    })
    public async getAll(@Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitals();
        if (hospitals === null) {
            logger.info(responseMessage.RESULT_NOT_FOUND);
            return res.status(200).send(hospitals);
        }
        return hospitals;
    }

    @HttpCode(200)
    @Get("/province")
    @ResponseSchema(Hospital)
    @OpenAPI({
        summary: "List reference hospital by province",
        description: "This route provide hospital by province query",
        responses: {
            "200": {
                description: "return list of hospital at province ${name}",
            },
        },
    })
    public async getProvince(@QueryParam("name") name: string, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitalsByProvince(name);

        if (typeof hospitals === "undefined" || hospitals.length == 0) {
            logger.info(responseMessage.RESULT_NOT_FOUND_ON_PROVINCE + ":" + name.toUpperCase());
            return res.status(200).send(hospitals);
        }
        return hospitals;
    }

    @HttpCode(200)
    @Get("/city")
    @OpenAPI({
        summary: "List reference hospital by city",
        description: "This route provide hospital by city query",
        responses: {
            "200": {
                description: "return list of hospital at city ${name}",
            },
        },
    })
    public async getCity(@QueryParam("name") name: string, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitalsByCity(name);

        if (typeof hospitals === "undefined" || hospitals.length == 0) {
            logger.info(responseMessage.RESULT_NOT_FOUND_ON_CITY + ":" + name.toUpperCase());
            return res.status(200).send(hospitals);
        }
        return hospitals;
    }
}
